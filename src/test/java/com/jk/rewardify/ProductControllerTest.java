package com.jk.rewardify;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jk.rewardify.entity.Product;
import com.jk.rewardify.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestRestTemplate template;

    @Test
    @WithUserDetails("admin")
    void givenProductObjectGiven_thenSaveAndReturnProduct() throws Exception {

        // given - precondition or setup
        var product = Product.builder()
                .id(UUID.randomUUID())
                .name("Mobile")
                .description("Apple")
                .price(BigDecimal.valueOf(100000))
                .build();

        // when - action or behaviour that we are going test
        ResultActions response = mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(product)));

        // then - verify the result or output using assert statements
        response.andDo(print()).
                andExpect(status().isCreated());
        /*
                .andExpect(jsonPath("$.name", is(product.getName())))
                .andExpect(jsonPath("$.price", is(product.getPrice())));*/

    }

    // JUnit test for Get All employees REST API
    @Test
    @WithUserDetails("admin")
    void givenListOfProducts_thenReturnProductList() throws Exception {
        // given - precondition or setup
        List<Product> products = new ArrayList<>();
        var product = Product.builder()
                .id(UUID.randomUUID())
                .name("Mobile")
                .description("Apple")
                .price(BigDecimal.valueOf(100000))
                .build();
        var product1 = Product.builder()
                .id(UUID.randomUUID())
                .name("Mobile")
                .description("Apple")
                .price(BigDecimal.valueOf(100000))
                .build();
        products.add(product);
        products.add(product1);
        productRepository.saveAll(products);
        // when -  action or the behaviour that we are going test
        ResultActions response = mockMvc.perform(get("/products"));

        // then - verify the output
        response.andExpect(status().isOk());
    }
}
