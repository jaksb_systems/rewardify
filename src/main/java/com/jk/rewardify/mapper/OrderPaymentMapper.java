package com.jk.rewardify.mapper;

import com.jk.rewardify.dto.Payment;
import com.jk.rewardify.entity.OrderPayment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

@Mapper(componentModel = "spring")
public interface OrderPaymentMapper {

    @Mapping(source = "orderId", target = "order.id")
    OrderPayment map(UUID orderId, Payment payment);
}
