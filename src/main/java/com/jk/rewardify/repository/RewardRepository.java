package com.jk.rewardify.repository;

import com.jk.rewardify.dto.RewardType;
import com.jk.rewardify.entity.Reward;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface RewardRepository extends CrudRepository<Reward, UUID> {

    @Query("select sum(r.point) from Reward r " +
            "WHERE r.order.user.id= :userId and r.rewardType= :rewardType and r.expiryAt >= CURRENT_DATE")
    Long findTotalEarnedPointByUserIdAndRewardType(@Param("userId") UUID userId, @Param("rewardType") RewardType rewardType);

    @Query("select sum(r.point) from Reward r WHERE r.order.user.id= :userId and r.rewardType= :rewardType")
    Long findTotalRedeemByUserIdAndRewardType(@Param("userId") UUID userId, @Param("rewardType") RewardType rewardType);

    List<Reward> findByOrder_User_IdAndRewardTypeOrderByCreatedAtDesc(UUID userId, RewardType rewardType);
}
