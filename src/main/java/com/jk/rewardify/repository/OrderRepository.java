package com.jk.rewardify.repository;

import com.jk.rewardify.dto.OrderStatus;
import com.jk.rewardify.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {

    @Modifying
    @Transactional
    @Query("update Order o set o.orderStatus = :orderStatus where o.id = :orderId")
    Long updateStatus(@Param("orderStatus") OrderStatus orderStatus, @Param("orderStatus") UUID orderId);
}
