package com.jk.rewardify.repository;

import com.jk.rewardify.entity.OrderPayment;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface OrderPaymentRepository extends CrudRepository<OrderPayment, UUID> {
}
