package com.jk.rewardify.repository;

import com.jk.rewardify.entity.RewardProduct;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface RewardProductRepository extends CrudRepository<RewardProduct, UUID> {

    List<RewardProduct> findByProduct_IdIn(List<UUID> productIds);
}
