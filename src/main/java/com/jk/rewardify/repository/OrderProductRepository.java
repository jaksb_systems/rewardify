package com.jk.rewardify.repository;

import com.jk.rewardify.entity.OrderProduct;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Map;
import java.util.UUID;

public interface OrderProductRepository extends CrudRepository<OrderProduct, UUID> {

    @Query("select op.product.name as productName, count(op.product.id) as numberOfProduct from OrderProduct op " +
            "group by op.product.id order by count(op.product.id) desc limit 1")
    Map<String, Object> getCountByProductId();
}
