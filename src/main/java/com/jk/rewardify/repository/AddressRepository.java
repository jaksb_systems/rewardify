package com.jk.rewardify.repository;

import com.jk.rewardify.entity.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface AddressRepository extends CrudRepository<Address, UUID> {

}
