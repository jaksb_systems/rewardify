package com.jk.rewardify.repository;

import com.jk.rewardify.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ProductRepository extends CrudRepository<Product, UUID> {

    List<Product> findByIdIn(List<UUID> ids);
}
