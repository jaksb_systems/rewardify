package com.jk.rewardify.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidPointException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidPointException(String message) {
        super(message);
    }

    public InvalidPointException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
