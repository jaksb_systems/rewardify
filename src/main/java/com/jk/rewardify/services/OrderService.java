package com.jk.rewardify.services;

import com.jk.rewardify.dto.OrderStatus;
import com.jk.rewardify.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface OrderService {

    Order createOrder(UUID userId, Order order);

    Order updateOrder(UUID id, Order order);

    Page<Order> findAll(Pageable pageable);

    Order findById(UUID orderId);

    Long updateStatus(OrderStatus paid, UUID orderId);
}
