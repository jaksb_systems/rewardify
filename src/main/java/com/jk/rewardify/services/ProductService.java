package com.jk.rewardify.services;

import com.jk.rewardify.entity.Product;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    Product createProduct(Product product);

    Product updateProduct(Product product);

    List<Product> getAllProduct();

    Product getProductById(UUID productId);

    void deleteProduct(UUID id);

    List<Product> findByProduct_IdIn(List<UUID> ids);
}
