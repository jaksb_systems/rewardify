package com.jk.rewardify.services;

import java.util.Map;
import java.util.UUID;

public interface ReportService {
    Map<String, Object> findNumberOfPointsEarned(UUID userId);

    Map<String, Object> findNumberOfRewardsRedeemed(UUID userId);

    Map<String, Object> findMostPopularProducts();
}
