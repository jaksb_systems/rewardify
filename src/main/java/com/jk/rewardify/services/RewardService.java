package com.jk.rewardify.services;

import com.jk.rewardify.dto.RewardType;
import com.jk.rewardify.entity.Order;
import com.jk.rewardify.entity.Reward;

import java.util.List;
import java.util.UUID;

public interface RewardService {

    List<Reward> findRewardByUserIdAndRewardType(UUID userId, String rewardType);

    Long findTotalReward(UUID userId);

    Reward saveReward(Order order, RewardType rewardType);
}
