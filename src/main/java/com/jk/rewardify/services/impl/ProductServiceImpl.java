package com.jk.rewardify.services.impl;

import com.jk.rewardify.entity.Product;
import com.jk.rewardify.exception.ResourceNotFoundException;
import com.jk.rewardify.repository.ProductRepository;
import com.jk.rewardify.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(Product product) {
        Optional<Product> productDb = this.productRepository.findById(product.getId());
        if (productDb.isPresent()) {
            Product productUpdate = productDb.get();
            productUpdate.setId(product.getId());
            productUpdate.setName(product.getName());
            productUpdate.setDescription(product.getDescription());
            productRepository.save(productUpdate);
            return productUpdate;
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + product.getId());
        }
    }

    @Override
    public List<Product> getAllProduct() {
        return (List<Product>) this.productRepository.findAll();
    }

    @Override
    public Product getProductById(UUID productId) {
        Optional<Product> productDb = this.productRepository.findById(productId);
        if (productDb.isPresent()) {
            return productDb.get();
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + productId);
        }
    }

    @Override
    public void deleteProduct(UUID productId) {
        Optional<Product> productDb = this.productRepository.findById(productId);
        if (productDb.isPresent()) {
            this.productRepository.delete(productDb.get());
        } else {
            throw new ResourceNotFoundException("Record not found with id : " + productId);
        }
    }

    @Override
    public List<Product> findByProduct_IdIn(List<UUID> ids) {
        return productRepository.findByIdIn(ids);
    }

}
