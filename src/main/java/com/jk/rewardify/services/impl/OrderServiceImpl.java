package com.jk.rewardify.services.impl;

import com.jk.rewardify.dto.OrderStatus;
import com.jk.rewardify.dto.RewardType;
import com.jk.rewardify.entity.Order;
import com.jk.rewardify.entity.Product;
import com.jk.rewardify.exception.ResourceNotFoundException;
import com.jk.rewardify.repository.OrderRepository;
import com.jk.rewardify.services.OrderService;
import com.jk.rewardify.services.ProductService;
import com.jk.rewardify.services.RewardService;
import com.jk.rewardify.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ProductService productService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private RewardService rewardService;
    @Autowired
    private UserService userService;

    @Override
    public Order createOrder(UUID userId, Order order) {
        order.setUser(userService.findById(userId));
        order.getOrderProducts().forEach(op -> op.setOrder(order));
        double total = getTotal(order);
        order.setTotal(BigDecimal.valueOf(total));
        order.setOrderNo(String.valueOf(System.nanoTime()));
        order.setOrderStatus(OrderStatus.CREATED);
        orderRepository.save(order);
        rewardService.saveReward(order, RewardType.EARNED);
        return order;
    }

    private double getTotal(Order order) {
        var ids = order.getOrderProducts().stream().map(op -> op.getProduct().getId()).collect(Collectors.toList());
        var products = productService.findByProduct_IdIn(ids);
        return order.getOrderProducts().stream()
                .mapToDouble(op -> products.stream()
                        .filter(p -> p.getId().equals(op.getProduct().getId()))
                        .findFirst().orElse(Product.builder().price(BigDecimal.ONE).build()).getPrice()
                        .multiply(BigDecimal.valueOf(op.getQuantity())).doubleValue())
                .sum();
    }

    @Override
    public Order updateOrder(UUID id, Order order) {
        var orderOpt = orderRepository.findById(id);
        if (orderOpt.isEmpty()) {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
        order.setId(id);
        return orderRepository.save(order);
    }

    @Override
    public Page<Order> findAll(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }

    @Override
    public Order findById(UUID orderId) {
        var orderOpt = orderRepository.findById(orderId);
        if (orderOpt.isEmpty()) {
            throw new ResourceNotFoundException("Record not found with id : " + orderId);
        }
        return orderOpt.get();
    }

    @Override
    public Long updateStatus(OrderStatus orderStatus, UUID orderId) {
        return orderRepository.updateStatus(orderStatus, orderId);
    }
}
