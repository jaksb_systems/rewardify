package com.jk.rewardify.services.impl;

import com.jk.rewardify.dto.OrderStatus;
import com.jk.rewardify.dto.Payment;
import com.jk.rewardify.dto.RewardType;
import com.jk.rewardify.entity.OrderPayment;
import com.jk.rewardify.exception.InvalidPointException;
import com.jk.rewardify.exception.ResourceNotFoundException;
import com.jk.rewardify.mapper.OrderPaymentMapper;
import com.jk.rewardify.repository.OrderPaymentRepository;
import com.jk.rewardify.services.OrderPaymentService;
import com.jk.rewardify.services.OrderService;
import com.jk.rewardify.services.RewardService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.UUID;

@Service
public class OrderPaymentServiceImpl implements OrderPaymentService {

    private final OrderPaymentMapper orderPaymentMapper = Mappers.getMapper(OrderPaymentMapper.class);
    @Autowired
    private OrderPaymentRepository orderPaymentRepository;
    @Autowired
    private RewardService rewardService;
    @Autowired
    private OrderService orderService;

    @Override
    @Transactional
    public OrderPayment makeOrderPayment(UUID orderId, Payment payment) {
        var order = orderService.findById(orderId);
        if (order == null) {
            throw new ResourceNotFoundException("Record not found with id : " + orderId);
        }
        var totalPayment = new BigDecimal(0);
        if (payment.getPoint() != null) {
            var totalPoint = rewardService.findTotalReward(order.getUser().getId());
            if (payment.getPoint() > totalPoint) {
                throw new InvalidPointException("totalPoint available is: " + totalPoint);
            }
            totalPayment = totalPayment.add(BigDecimal.valueOf(payment.getPoint()));
        }
        if (payment.getAmount() != null) {
            totalPayment = totalPayment.add(payment.getAmount());
        }
        var orderPayment = orderPaymentMapper.map(orderId, payment);
        if (order.getTotal().compareTo(totalPayment) != 0) {
            throw new InvalidPointException("Need total payment of: " + order.getTotal());
        }
        orderPaymentRepository.save(orderPayment);
        rewardService.saveReward(order, RewardType.REDEEMED);
        orderService.updateStatus(OrderStatus.PAID, orderId);
        return orderPayment;
    }

}
