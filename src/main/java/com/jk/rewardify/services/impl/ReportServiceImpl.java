package com.jk.rewardify.services.impl;

import com.jk.rewardify.dto.RewardType;
import com.jk.rewardify.repository.OrderProductRepository;
import com.jk.rewardify.repository.RewardRepository;
import com.jk.rewardify.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.UUID;


@Service
@Transactional(readOnly = true)
public class ReportServiceImpl implements ReportService {

    @Autowired
    private OrderProductRepository orderProductRepository;

    @Autowired
    private RewardRepository rewardRepository;

    @Override
    public Map<String, Object> findNumberOfPointsEarned(UUID userId) {
        var totalEarned = rewardRepository.findTotalEarnedPointByUserIdAndRewardType(userId, RewardType.EARNED);
        return Map.of("totalEarnedPoint", totalEarned == null ? 0 : totalEarned);
    }

    @Override
    public Map<String, Object> findNumberOfRewardsRedeemed(UUID userId) {
        var totalRedeem = rewardRepository.findTotalRedeemByUserIdAndRewardType(userId, RewardType.REDEEMED);
        return Map.of("totalRedeemPoint", totalRedeem == null ? 0 : totalRedeem);
    }

    @Override
    public Map<String, Object> findMostPopularProducts() {
        return orderProductRepository.getCountByProductId();
    }
}
