package com.jk.rewardify.services.impl;

import com.jk.rewardify.entity.User;
import com.jk.rewardify.exception.ResourceNotFoundException;
import com.jk.rewardify.repository.UserRepository;
import com.jk.rewardify.services.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public User createUser(User user) {
        user.getAddressList().forEach(a -> a.setUser(user));
        userRepository.save(user);
        log.debug("User create");
        return user;
    }

    @Override
    @Transactional
    public User updateUser(UUID id, User user) {
        var optUser = userRepository.findById(id);
        if (optUser.isEmpty()) {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
        user.setId(id);
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll(Pageable pageable) {
        var userList = new ArrayList<User>();
        var users = userRepository.findAll();
        users.forEach(u -> {
            if (u.getAddressList() != null) {
                u.getAddressList().forEach(a -> a.setUser(null));
            }
            userList.add(u);
        });
        return userList;
    }

    @Override
    public User findById(UUID userId) {
        var optUser = userRepository.findById(userId);
        if (optUser.isEmpty()) {
            throw new ResourceNotFoundException("Record not found with id : " + userId);
        }
        return optUser.get();
    }
}
