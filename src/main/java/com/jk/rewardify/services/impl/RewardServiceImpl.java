package com.jk.rewardify.services.impl;

import com.jk.rewardify.dto.RewardType;
import com.jk.rewardify.entity.Order;
import com.jk.rewardify.entity.Reward;
import com.jk.rewardify.entity.RewardProduct;
import com.jk.rewardify.repository.RewardProductRepository;
import com.jk.rewardify.repository.RewardRepository;
import com.jk.rewardify.services.RewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static com.jk.rewardify.util.AppConstant.REWARD_EXPIRY;

@Service
@Transactional(readOnly = true)
public class RewardServiceImpl implements RewardService {

    @Autowired
    private RewardRepository rewardRepository;

    @Autowired
    private RewardProductRepository rewardProductRepository;

    @Override
    public List<Reward> findRewardByUserIdAndRewardType(UUID userId, String rewardType) {
        return rewardRepository.findByOrder_User_IdAndRewardTypeOrderByCreatedAtDesc(userId, RewardType.of(rewardType));
    }

    @Override
    public Long findTotalReward(UUID userId) {
        var totalEarnedPoint = rewardRepository.findTotalEarnedPointByUserIdAndRewardType(userId, RewardType.EARNED);
        var redeemPoint = rewardRepository.findTotalRedeemByUserIdAndRewardType(userId, RewardType.REDEEMED);
        return totalEarnedPoint - redeemPoint;
    }

    @Override
    public Reward saveReward(Order order, RewardType rewardType) {
        var productIds = order.getOrderProducts().stream()
                .map(op -> op.getProduct().getId())
                .collect(Collectors.toList());
        var rewardProducts =
                rewardProductRepository.findByProduct_IdIn(productIds);
        var totalPoint = getTotalPoint(order, rewardProducts);
        var futureDate = LocalDate.now().plusMonths(REWARD_EXPIRY);
        var reward = new Reward();
        reward.setRewardType(rewardType);
        reward.setPoint(totalPoint.get());
        reward.setOrder(order);
        reward.setExpiryAt(futureDate);
        return rewardRepository.save(reward);
    }

    private AtomicLong getTotalPoint(Order order, List<RewardProduct> rewardProducts) {
        var totalPoint = new AtomicLong();
        order.getOrderProducts().forEach(op -> {
            var point = rewardProducts.stream()
                    .filter(rp -> rp.getProduct().getId().equals(op.getProduct().getId()))
                    .mapToLong(RewardProduct::getReward).findFirst().orElse(0);
            totalPoint.addAndGet(op.getQuantity() * point);
        });
        return totalPoint;
    }
}
