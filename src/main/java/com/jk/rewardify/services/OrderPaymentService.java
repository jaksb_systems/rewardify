package com.jk.rewardify.services;

import com.jk.rewardify.dto.Payment;
import com.jk.rewardify.entity.OrderPayment;

import java.util.UUID;

public interface OrderPaymentService {
    OrderPayment makeOrderPayment(UUID orderId, Payment payment);
}
