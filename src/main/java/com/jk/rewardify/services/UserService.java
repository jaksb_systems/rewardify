package com.jk.rewardify.services;

import com.jk.rewardify.entity.User;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface UserService {

    User createUser(User user);

    User updateUser(UUID id, User user);

    List<User> findAll(Pageable pageable);

    User findById(UUID userId);
}
