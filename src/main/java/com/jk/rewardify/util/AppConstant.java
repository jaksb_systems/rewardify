package com.jk.rewardify.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AppConstant {

    public static final int REWARD_EXPIRY = 12; //EXPIRY in months
    public static final String SECRET = "secret-key-12345";
    public static final String AES = "AES";
}
