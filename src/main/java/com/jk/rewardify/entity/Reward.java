package com.jk.rewardify.entity;

import com.jk.rewardify.dto.RewardType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "reward")
public class Reward implements Serializable {

    @Id
    private UUID id;

    //rewardType can earn or redeem
    @Enumerated(EnumType.STRING)
    @Column(name = "reward_type")
    private RewardType rewardType;

    @Column(name = "point")
    private Long point;

    @Column(name = "expiry_at", updatable = false)
    private LocalDate expiryAt;

    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    @PrePersist
    public void prePersist() {
        setId(UUID.randomUUID());
        setCreatedAt(LocalDateTime.now());
    }

}
