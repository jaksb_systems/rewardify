package com.jk.rewardify.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "reward_product")
public class RewardProduct implements Serializable {

    @Id
    private UUID id;

    @Column(name = "reward")
    private Integer reward;

    //maximum percentage can be redeem for this product
    @Column(name = "max_redeem_per")
    private Integer maxRedeemPer;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @Column(name = "created_at", updatable = false)
    private LocalDateTime createAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @PrePersist
    public void prePersist() {
        setId(UUID.randomUUID());
        setCreateAt(LocalDateTime.now());
    }

    @PreUpdate
    public void preUpdate() {
        setUpdatedAt(LocalDateTime.now());
    }

}
