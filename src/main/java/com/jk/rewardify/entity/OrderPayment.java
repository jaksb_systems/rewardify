package com.jk.rewardify.entity;

import com.jk.rewardify.config.AttributeEncryptor;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "order_payment")
public class OrderPayment implements Serializable {

    @Id
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "point")
    private Long point;

    @Column(name = "method", nullable = false)
    private String method;

    @Column(name = "reference", nullable = false)
    private String reference;

    @Convert(converter = AttributeEncryptor.class)
    @Column(name = "card_no")
    private String cardNo;

    @Column(name = "name_on_card")
    private String nameOnCard;

    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @PrePersist
    public void prePersist() {
        setId(UUID.randomUUID());
        setCreatedAt(LocalDateTime.now());
    }

}
