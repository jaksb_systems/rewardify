package com.jk.rewardify.dto;

import java.util.stream.Stream;

public enum OrderStatus {

    CREATED("Created"), PAYMENT_PENDING("Payment_Pending"), PAID("Paid"), COMPLETE("Complete");

    private final String status;

    OrderStatus(String status) {
        this.status = status;
    }

    public static OrderStatus of(String status) {
        return Stream.of(OrderStatus.values())
                .filter(p -> p.getStatus().equals(status))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public String getStatus() {
        return this.status;
    }
}
