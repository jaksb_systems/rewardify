package com.jk.rewardify.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class Payment implements Serializable {

    private BigDecimal amount;

    private String method;

    private String reference;

    private String cardNo;

    private String nameOnCard;

    private Long point;
}
