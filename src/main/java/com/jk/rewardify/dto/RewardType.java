package com.jk.rewardify.dto;

import java.util.stream.Stream;

public enum RewardType {

    EARNED("Earn"), REDEEMED("Redeem");

    private final String type;

    RewardType(String type) {
        this.type = type;
    }

    public static RewardType of(String type) {
        return Stream.of(RewardType.values())
                .filter(p -> p.getType().equals(type))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public String getType() {
        return this.type;
    }
}
