package com.jk.rewardify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class RewardifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RewardifyApplication.class, args);
    }

}
