package com.jk.rewardify.controller;

import com.jk.rewardify.dto.Payment;
import com.jk.rewardify.entity.OrderPayment;
import com.jk.rewardify.services.OrderPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class OrderPaymentController {

    @Autowired
    private OrderPaymentService orderPaymentService;

    @PostMapping("/order/{orderId}/payments")
    public ResponseEntity<OrderPayment> orderPayment(@PathVariable UUID orderId, Payment payment) {
        var responseMap = orderPaymentService.makeOrderPayment(orderId, payment);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseMap);
    }

}
