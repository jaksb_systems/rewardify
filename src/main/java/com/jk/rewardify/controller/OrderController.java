package com.jk.rewardify.controller;

import com.jk.rewardify.entity.Order;
import com.jk.rewardify.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/orders")
    public ResponseEntity<Page<Order>> findAll(@RequestParam("pageNo") int pageNo,
                                               @RequestParam(value = "size", defaultValue = "10") int size) {
        var pageable = PageRequest.of((pageNo > 1 ? pageNo - 1 : 0), size);
        return ResponseEntity.ok().body(orderService.findAll(pageable));
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getById(@PathVariable UUID id) {
        return ResponseEntity.ok().body(orderService.findById(id));
    }

    @PostMapping("/orders/{userId}")
    public ResponseEntity<Order> create(@PathVariable UUID userId, @RequestBody Order order) {
        var dbOrder = orderService.createOrder(userId, order);
        dbOrder.getOrderProducts().forEach(op -> op.setOrder(null));
        return ResponseEntity.status(HttpStatus.CREATED).body(dbOrder);

    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Order> update(@PathVariable UUID id, @RequestBody Order order) {
        var dbOrder = orderService.updateOrder(id, order);
        dbOrder.getOrderProducts().forEach(op -> op.setOrder(null));
        return ResponseEntity.ok().body(dbOrder);
    }
}
