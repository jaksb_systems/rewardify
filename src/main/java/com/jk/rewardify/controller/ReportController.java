package com.jk.rewardify.controller;

import com.jk.rewardify.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.UUID;

@RestController
public class ReportController {

    @Autowired
    private ReportService reportService;

    //@PreAuthorize("hasRole('ADMIN'")
    @GetMapping("/reports/numberOfPointsEarned/{userId}")
    public ResponseEntity<Map<String, Object>> findNumberOfPointsEarned(@PathVariable UUID userId) {
        var responseMap = reportService.findNumberOfPointsEarned(userId);
        return ResponseEntity.ok().body(responseMap);
    }

    //@PreAuthorize("hasRole('ADMIN'")
    @GetMapping("/reports/numberOfRewardsRedeemed/{userId}")
    public ResponseEntity<Map<String, Object>> findNumberOfRewardsRedeemed(@PathVariable UUID userId) {
        var responseMap = reportService.findNumberOfRewardsRedeemed(userId);
        return ResponseEntity.ok().body(responseMap);
    }

    //@PreAuthorize("hasRole('ADMIN'")
    @GetMapping("/reports/mostPopularProducts")
    public ResponseEntity<Map<String, Object>> findMostPopularProducts() {
        var responseMap = reportService.findMostPopularProducts();
        return ResponseEntity.ok().body(responseMap);
    }
}
