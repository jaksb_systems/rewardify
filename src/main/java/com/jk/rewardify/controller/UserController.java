package com.jk.rewardify.controller;

import com.jk.rewardify.entity.User;
import com.jk.rewardify.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> findAll(
            @RequestParam("pageNo") int pageNo, @RequestParam(value = "size", defaultValue = "10")
    int size) {
        Pageable pageable = PageRequest.of((pageNo > 1 ? pageNo - 1 : 0), size);
        return ResponseEntity.ok().body(userService.findAll(pageable));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> findById(@PathVariable UUID id) {
        return ResponseEntity.ok().body(userService.findById(id));
    }

    @PostMapping("/users")
    public ResponseEntity<User> create(@RequestBody User user) {
        var dbUser = userService.createUser(user);
        dbUser.setAddressList(null);
        return ResponseEntity.status(HttpStatus.CREATED).body(dbUser);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> update(@PathVariable UUID id, @RequestBody User user) {
        return ResponseEntity.ok().body(userService.updateUser(id, user));
    }

}
