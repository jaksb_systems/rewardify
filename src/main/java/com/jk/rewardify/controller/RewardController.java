package com.jk.rewardify.controller;

import com.jk.rewardify.entity.Reward;
import com.jk.rewardify.services.RewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class RewardController {

    @Autowired
    private RewardService rewardService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/rewards/orders/users/{userId}")
    public ResponseEntity<List<Reward>> findRewardByUserIdAndRewardType(@PathVariable UUID userId,
                                                                        @RequestParam("rewardType") String rewardType) {
        return ResponseEntity.ok().body(rewardService.findRewardByUserIdAndRewardType(userId, rewardType));
    }

    @GetMapping("/rewards/orders/users/{userId}/points")
    public ResponseEntity<Long> findTotalReward(@PathVariable UUID userId) {
        return ResponseEntity.ok().body(rewardService.findTotalReward(userId));
    }
}
